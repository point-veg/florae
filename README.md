
<!-- README.md is generated from README.Rmd. Please edit that file -->

# florae

<!-- badges: start -->
<!-- badges: end -->

Consult some well-known websites providing plant checklists, namely:

1)  Euro+Med1 <http://ww2.bgbm.org/EuroPlusMed>

2)  Flora iberica <http://www.floraiberica.es>, and

3)  Flora-On <https://flora-on.pt>.

(Euro+Med2 <http://www.europlusmed.org> is also under development.)

The retrieving of the websites information is performed (in a dirty way)
by inspecting the html output and not via API.

## Acknowledgments

TMH was funded by the European Social Fund (POCH and NORTE 2020) and by
National Funds (MCTES), through a FCT – Fundação para a Ciência a
Tecnologia (Portuguese Foundation for Science and Technology)
postdoctoral fellowship (SFRH/BPD/115057/2016), as well as by National
Funds, through the same foundation, under the project UIDB/04033/2020
(CITAB - Centre for the Research and Technology of Agro-Environmental
and Biological Sciences).

## Installation

You can install the package from GitLab.

``` r
devtools::install_git("https://gitlab.com/point-veg/florae")
```
