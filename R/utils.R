#clean leading, trailing and multiple "white spaces"
clean_whitespaces <- function(string) {return(trimws(gsub("\\s+", " ", as.character(string))))}

#clean stuff on demand
clean_stuff <- function(string, stuff=NULL) {
  if (!is.null(stuff) & length(stuff)>0) {
    string <- as.character(string)
    for (i in 1:length(stuff)) {
      string <- gsub(stuff[i], "", string)
    }
  }
  return(string)
}

#splits a list after given breaks
split_list <- function (inner.breaks, list) {
  break.points <- c(Ini=0, inner.breaks, End=length(list)+1)
  cut.factor <- cut(1:length(list), breaks=unique(break.points))
  return(split(list, cut.factor))
}

###########################################
#auxiliary functions to EURO+MED functions#
###########################################

#auxiliary function to outputs
return_auxiliary <- function(search_name = NA, NameId = NA, found_name = NA, status = NA, synonym_of = NA, applied_to = NA, accepted_EURO_PLUS_MED_name = NA, accepted_EURO_PLUS_MED_ID = NA, accepted_EURO_PLUS_MED_occurrence = NA, report, has.occurrence = NA, occurrence = NA, sources = NA, consensus = NA, other_names = NA, relevant.region = NULL, full.output = FALSE) {

  if (full.output) { #full output
    NULL_to_NA <- function (x) {if (length(x)==0) {x <- NA}; return(x)}
    search_name <- NULL_to_NA(search_name)
    NameId <- NULL_to_NA(NameId)
    found_name <- NULL_to_NA(found_name)
    status <- NULL_to_NA(status)
    synonym_of <- NULL_to_NA(synonym_of)
    applied_to <- NULL_to_NA(applied_to)
    accepted_EURO_PLUS_MED_name <- NULL_to_NA(accepted_EURO_PLUS_MED_name)
    accepted_EURO_PLUS_MED_ID <- NULL_to_NA(accepted_EURO_PLUS_MED_ID)
    accepted_EURO_PLUS_MED_occurrence <- NULL_to_NA(accepted_EURO_PLUS_MED_occurrence)
    report <- NULL_to_NA(report)
    has.occurrence <- NULL_to_NA(has.occurrence)
    occurrence <- NULL_to_NA(occurrence)
    sources <- NULL_to_NA(sources)
    consensus <- NULL_to_NA(consensus)
    other_names <- NULL_to_NA(other_names)
    if (!is.null(relevant.region) & !is.na(occurrence)) {
      if (!is_present(occurrence, region = relevant.region)) {
        report <- paste0(report, " Attention! Returned taxon '", accepted_EURO_PLUS_MED_name, "' is not present in the given relevant.region '", relevant.region,"'.")
      }
    }
    return(c(
      search_name = search_name,
      NameId = NameId,
      found_name = found_name,
      status = status,
      synonym_of = synonym_of,
      applied_to = applied_to,
      accepted_EURO_PLUS_MED_name = accepted_EURO_PLUS_MED_name,
      accepted_EURO_PLUS_MED_ID = accepted_EURO_PLUS_MED_ID,
      accepted_EURO_PLUS_MED_occurrence = accepted_EURO_PLUS_MED_occurrence,
      report = report,
      has.occurrence = has.occurrence,
      occurrence = occurrence,
      sources = sources,
      consensus = consensus,
      other_names = other_names
    ))
  } else { #short output
    NULL_to_NA <- function (x) {if (length(x)==0) {x <- NA}; return(x)}
    accepted_EURO_PLUS_MED_name <- NULL_to_NA(accepted_EURO_PLUS_MED_name)
    accepted_EURO_PLUS_MED_ID <- NULL_to_NA(accepted_EURO_PLUS_MED_ID)
    accepted_EURO_PLUS_MED_occurrence <- NULL_to_NA(accepted_EURO_PLUS_MED_occurrence)
    report <- NULL_to_NA(report)

    if (!is.null(relevant.region) & !is.na(occurrence)) {
      if (!is_present(occurrence, region = relevant.region)) {
        report <- paste0(report, " Attention! Returned taxon '", accepted_EURO_PLUS_MED_name, "' is not present in the given relevant.region '", relevant.region,"'.")
      }
    }
    return(c(
      accepted_EURO_PLUS_MED_name = accepted_EURO_PLUS_MED_name,
      accepted_EURO_PLUS_MED_ID = accepted_EURO_PLUS_MED_ID,
      accepted_EURO_PLUS_MED_occurrence = accepted_EURO_PLUS_MED_occurrence,
      report = report
    ))
  }
}

#get the EURO+MED ID from an URL string
get_IDfromHTML <- function(URL) {
  if (length(URL)==0) {return(NA)}
  IDtemp <- sub("\\\">.*", "", URL)
  IDfinal <- sub(".*NameId=", "", IDtemp)
  return(as.character(IDfinal))
}


###############################
### select.among.finalnames ###
###############################

#auxiliary function to EURO+MED functions (creates a table of options to present to the user)

select_among_final_names <- function(res, final_names, relevant.region) {
  if (length(res) == 0) {
    return(NA)
    #return(c('Name:' = NA, 'Status:' = NA, 'Final name:' = NA, 'Possible misapplied name?' = NA, 'In relevant.region?' = NA, 'NameId:' = NA, 'Nomencl. ref.:' = NA, 'Misapplied names:' = NA, 'Occurrence:' = NA))
  }
  table.to.print <- matrix(NA, 0, 9, dimnames = list(NULL, c('Name:', 'Status:', 'Final name:', 'Possible misapplied name?', 'In relevant.region?', 'NameId:', 'Nomencl. ref.:', 'Misapplied names:', 'Occurrence:')))
  res.final <- list()
  if (length(res) != length(final_names)) {stop("Recoding definitely needed here!")}

  for (i in 1:length(res)) {
    res.temp <- res[[i]]
    if (!is.null(relevant.region) & !is.null(res.temp$`Occurrence:`)) {
      occur.in.relevant.region <- is_present(res.temp$`Occurrence:`, relevant.region)
    } else {
      occur.in.relevant.region <- NA
    }

    if (is.null(res.temp$`Nomencl. ref.:`)) {
      res.temp$`Nomencl. ref.:` <- NA
    }
    if (is.null(res.temp$`Misapplied names:`)) {
      res.temp$`Misapplied names:` <- NA
    }
    if (length(res.temp$`Misapplied names:`) > 1) {
      res.temp$`Misapplied names:` <- paste0(res.temp$`Misapplied names:`, collapse = " OR ")
    }
    if (is.null(res.temp$`Status:`)) {
      res.temp$`Status:` <- NA
    }

    #Possible misapplied name! (search in "http://www.europlusmed.org" to try to find it manually)
    if ((res.temp$`Status:` == "ACCEPTED" | res.temp$`Status:` == "PRELIMINARY ACCEPTED") & is.null(res.temp$'Occurrence:')) {
      if (!is.null(res.temp$`NameId:`)) {stop("Unexpected presence of 'NameId'. Check this case and possibly recode.")}
      table.to.print <- rbind(table.to.print, c('Name:' = res.temp$'Name:', 'Status:' = res.temp$'Status:', 'Final name:' = final_names[i], 'Possible misapplied name?' = "YES, check europlusmed.org", 'In relevant.region?' = NA, 'NameId:' = NA, 'Nomencl. ref.:' = res.temp$`Nomencl. ref.:`, 'Misapplied names:' = res.temp$`Misapplied names:`, 'Occurrence:' = NA))
      res.final[[i]] <- res.temp
      next
    }

    #This is a possible choice (that can be restricted to the relevant.region)
    if (res.temp$`Status:` == "ACCEPTED" & !is.null(res.temp$`Occurrence:`) & !is.null(res.temp$`NameId:`)) {
      table.to.print <- rbind(table.to.print, c('Name:' = res.temp$'Name:', 'Status:' = res.temp$'Status:', 'Final name:' = final_names[i], 'Possible misapplied name?' = "No/unknown.", 'In relevant.region?' = occur.in.relevant.region, 'NameId:' = res.temp$`NameId:`, 'Nomencl. ref.:' = res.temp$`Nomencl. ref.:`, 'Misapplied names:' = res.temp$`Misapplied names:`, 'Occurrence:' = res.temp$`Occurrence:`))
      res.final[[i]] <- res.temp
      next
    }

    #Consult Euro+Med for other final_names (possibly ACCEPTED or from other Euro+Med versions), returning those ACCEPTED
    if (!is.null(res.temp$`NameId:`) & is.null(res.temp$`Occurrence:`)) {
      consult <- EUROMED_accepted_name(search.name = NULL, NameId = res.temp$`NameId:`, relevant.region= relevant.region, interactive=FALSE)

      #This is a possible choice (that can be restricted to the relevant.region)
      if (grepl("Successful.", consult["report"])) {
        if (!is.null(relevant.region)) { # & !is.na(consult["occurrence"]) should not be necessary
          consult_occur.in.relevant.region <- is_present(consult["accepted_EURO_PLUS_MED_occurrence"], region = relevant.region)
          consult_occur <- consult[["accepted_EURO_PLUS_MED_occurrence"]]
        } else {
          consult_occur.in.relevant.region <- NA
          consult_occur <- consult[["accepted_EURO_PLUS_MED_occurrence"]]
        }
        if (is.null(res.temp$`Occurrence:`)) {
          res.temp$`Occurrence:` <- NA
        }
        table.to.print <- rbind(table.to.print, c('Name:' = res.temp$'Name:', 'Status:' = res.temp$'Status:', 'Final name:' = final_names[i], 'Possible misapplied name?' = "No/unknown.", 'In relevant.region?' = consult_occur.in.relevant.region, 'NameId:' = res.temp$`NameId:`, 'Nomencl. ref.:' = res.temp$`Nomencl. ref.:`, 'Misapplied names:' = res.temp$`Misapplied names:`, 'Occurrence:' = consult_occur))
        res.final[[i]] <- res.temp
      }
      #This is probably coming from an old Euro+Med version. Not taken into consideration now. Don't add anything to table.to.print
      #if (consult["report"] == "No recognizable final sources. Possibly check name spelling.") {
      #	table.to.print <- rbind(table.to.print, c(`Name:` = res.temp$`Name:`, 'Final name:' = final_names[i], `Possible misapplied name?` = "Unknown. Maybe in a former Euro+Med version.", 'In relevant.region?' = NA))
      #}
    }
    if (is.null(res.temp$`NameId:`)) {stop("NameId was expected to be present. Check this case and possibly recode.")}
  }
  #return(table.to.print)
  return(unique(table.to.print)) #inserted unique here after removing from final.names() internal auxiliary function of EUROMED_accepted_name()
}

#auxiliary function that corrects the endemic symbol (CES)
CES <- function(x) {
  if (length(x)==0) {return(NULL)}
  x <- gsub("&#9679;", intToUtf8("9679"), x)
  return(x)
}

#other functions that can go exported one day

# gets occurrence string part with native information
get_native_string <- function(occurrence) {
  occurrence <- sub("\u25cf ?", "", occurrence)
  if (!grepl("\\^[.*\\]$", occurrence)) {native <- as.character(sub(" *\\[.*", "", occurrence))} else {native <- ""} #it assumes all non-native info is inside single square brackets at the end of the string
  return(native)
}

# gets occurrence string part with non-native information
get_non_native_string <- function(occurrence) {
  if (grepl("\\[", occurrence)) {non.native <- as.character(sub("\\]", "", sub(".*\\[", "", occurrence)))} else {non.native=""} #it assumes all non-native info is inside single square brackets
  return(non.native)
}

# breaks native or non.native string parts in elements
occurrence_elements <- function(string) {
  if (string %in% "") {return("")}
  string <- clean_whitespaces(string)
  string.temp <- paste0(string, " ")
  if (gsub("[dnac\u2020?-]*[[:upper:]]{1,2}[[:lower:]]{0,2}(\\([A-Z ]*\\))? ", "", string.temp) != "") {stop("Occurrence string structure seems corrupted (e.g. unknown prefixes, upper or lower cases in wrong position or number).")}
  breaks <- as.vector(gregexpr("[dnac\u2020?-]*[[:upper:]]{1,2}[[:lower:]]{0,2}(\\([A-Z ]*\\))? ", string.temp)[[1]])
  return(substring(string, breaks, c((breaks-2)[-1], nchar(string))))
}

# removes known Euro+Med prefixes from the native part or from the non native part of a occurrence string, or a vector of elements, or a single element
clean_prefixes <- function(string) {
  sapply(string, function (x) {
    if (x %in% "") {return("")}
    x <- clean_whitespaces(x)
    x.temp <- paste0(x, " ")
    if (gsub("[dnac\u2020?-]*[[:upper:]]{1,2}[[:lower:]]{0,2}(\\([A-Z ]*\\))? ", "", x.temp) != "") {stop("Occurrence string/elements structure seems corrupted (e.g. unknown prefixes, upper or lower cases in wrong position or number).")}
    return(gsub("([dnac\u2020?-]*)([[:upper:]]{1,2}[[:lower:]]{0,2}(\\([A-Z ]*\\))?)","\\2", x))
  }, USE.NAMES=FALSE)
}

# gets known Euro+Med prefixes from a single element
get_prefixes <- function(string) {
  if (length(occurrence_elements(string))>1) {stop("Your string contains more than one element.")}
  string <- clean_whitespaces(string)
  if (!grepl("^[dnac\u2020?-]*[[:upper:]]+", string)) {stop("Occurrence string beginning seems corrupted.")}
  string.temp <- paste0(string, " ")
  if (gsub("^[dnac\u2020?-]*[[:upper:]]{1,2}[[:lower:]]{0,2}(\\([A-Z ]*\\))? ", "", string.temp) != "") {stop("Occurrence string structure seems corrupted (e.g. unknown prefixes, upper or lower cases in wrong position or number).")}
  return(gsub("(^[dnac\u2020?-]*)([[:upper:]]{1,2}[[:lower:]]{0,2}(\\([A-Z ]*\\))? )","\\1", string.temp))
}

# gets the region from a single element (including prefixes!)
get_region <- function (string) {
  if (length(occurrence_elements(string))>1) {stop("Your string contains more than one element.")}
  return(sub("\\(.*", "", string))
}

# gets the subregion(s) from a single element
get_subregions <- function (string) {
  if (length(occurrence_elements(string))>1) {stop("Your string contains more than one element.")}
  inner.string <- gsub(".*\\(|\\).*", "", string)
  if (inner.string == string) {return("")}
  return(occurrence_elements(inner.string))
}

# gets the result of occurrence_elements() and separates each subregion, if present, in one single element with structure: "Region(subregion)"
#ATTENTION: order of elements might be changed
separate_subregions_elements <- function(occur.elemen) {
  reg.with.subreg <- sapply(occur.elemen, function (x) {grepl("\\(.*\\)", x)}, USE.NAMES=FALSE)
  res2 <- unlist(sapply(occur.elemen[reg.with.subreg], function (x) {
    paste0(get_region(x), "(", get_subregions(x), ")")	#here get_region() is needed returning prefixes
  }, USE.NAMES=FALSE))
  res1 <- occur.elemen[!reg.with.subreg]
  return(sort(c(res1, res2)))
}


# checks the validity of an EURO+MED occurrence string
validate_occurrence <- function(occurrence) {
  if (occurrence %in% "") {warning("Your occurrence string is NULL."); return(NULL)}
  native <- get_native_string(occurrence) #gets "native part"
  non.native <- get_non_native_string(occurrence) #gets "non native part"
  #checks for repetitions (with warning) e.g. "presence" and "reported in error" in the same region/subregion (with warning), but separately for the "native part" and the "introduced part".
  native.elements <- occurrence_elements(native)
  non.native.elements <- occurrence_elements(non.native)
  #remove os prefixos e vê se há repetidos
  if (any(duplicated(clean_prefixes(native.elements)))) {stop("There are duplicated regions in the 'native' part of the string.")}
  if (any(duplicated(clean_prefixes(non.native.elements)))) {stop("There are duplicated regions in the 'non-native' part of the string.")}
  if (any(duplicated(c(native.elements, non.native.elements)))) {warning("There are elements in the 'native' part of the string equally present in the 'non-native' part of the string. Possibly infrataxa have different occurrence status.")}
  if (native.elements[1] != "") {
    clean.native.regions <- clean_prefixes(sapply(native.elements, get_region, USE.NAMES=FALSE))
    validated.native.regions <- clean.native.regions %in% REGIONS
    if (!all(validated.native.regions)) {
      stop(paste(native.elements[!validated.native.regions], "not recognized as a valid region(s)."))
    }
    native.subelements <- separate_subregions_elements(native.elements)
    NATreg.with.subreg <- sapply(native.subelements, function (x) {grepl("\\(.*\\)", x)}, USE.NAMES=FALSE)
    sub.res <- NULL
    for (elem in native.subelements[NATreg.with.subreg]) {
      sub.res <- c(sub.res, all(get_subregions(elem) %in% SUBREGIONS[[clean_prefixes(get_region(elem))]]))
    }
    if (!all(sub.res)) {stop("Invalid subregions detected.")}
  } else {clean.native.regions=""}
  if (non.native.elements[1] != "") {
    clean.non.native.regions <- clean_prefixes(sapply(non.native.elements, get_region, USE.NAMES=FALSE))
    validated.non.native.regions <- clean.non.native.regions %in% REGIONS
    if (!all(validated.non.native.regions)) {
      stop(paste(non.native.elements[!validated.non.native.regions], "not recognized as a valid region(s)."))
    }
    non.native.subelements <- separate_subregions_elements(non.native.elements)
    NONreg.with.subreg <- sapply(non.native.subelements, function (x) {grepl("\\(.*\\)", x)}, USE.NAMES=FALSE)
    sub.res <- NULL
    for (elem in non.native.subelements[NONreg.with.subreg]) {
      sub.res <- c(sub.res, all(get_subregions(elem) %in% SUBREGIONS[[clean_prefixes(get_region(elem))]]))
    }
    if (!all(sub.res)) {stop("Invalid subregions detected.")}
  } else {clean.non.native.regions=""}
  return(invisible(list(native=native, non.native=non.native, native.elements=native.elements, non.native.elements=non.native.elements, clean.native.regions=clean.native.regions, clean.non.native.regions=clean.non.native.regions)))
}

# given an occurrence string, checks nativeness to a region or a region+subregion (with options)
# inc.doubtfully.native, inc.doubtfully.present, inc.extinct are combined using OR
# for the cases of regions with subregions, when a subregion is not given, the function returns TRUE even if the
# occurrence string contains only part of all the possible subregions of the region as native

is_native <- function(occurrence, region, sub.region=NULL, inc.doubtfully.native=FALSE, inc.doubtfully.present=FALSE, inc.extinct=FALSE) {
  if (length(occurrence)==0) {return(NULL)}
  if (is.na(occurrence)) {return(NA)}
  if (!region %in% REGIONS) {stop("Given region was not recognized as an EURO+MED region.")}
  if (!is.null(sub.region)) {
    validate_occurrence(paste0(region, "(", sub.region, ")"))
  }
  parts <- validate_occurrence(occurrence)
  if (any(grepl(region, parts$native.elements))) { #taxa apparently native in the region (but further checks needed)
    element <- grep(region, parts$native.elements, value=TRUE)
    if (length(element) > 1) { #the region appears more than once in the native part of the occurrence string (e.g. is doubtfully present in a subregion and native in other subregion, as "Glebionis segetum" in "Ga")
      ele.prefixes <- sapply(element, get_prefixes)
      ele.region <- sapply(element, function (x) {clean_prefixes(get_region(x))})
      ele.subregions <- sapply(element, get_subregions)
      if (!all(ele.region %in% region)) {
        stop("Unexpected result in 'is_native' function (1).")
      }
      if (is.null(sub.region)) { #without a given subregion
        if (any(region %in% element)) { #if so, no subregions and no prefixes in occurrence element (but this is not expected as multiple elements are most probably coming from different subregions present with different prefixes)
          return(TRUE)
        }
        if (any(ele.prefixes %in% "")) { #some elements with no prefixes (even if it has subregions)
          return(TRUE)
        } else { #all occurrence element have prefixes (even if it has subregions)
          #apparently this works as below, as grepl is vectorized and inc.doubtfully.native, inc.doubtfully.present and inc.extinct are recycled!
          is.doubtfully.native <- grepl("d", ele.prefixes)
          is.doubtfully.present <- grepl("[?]", ele.prefixes)
          is.extinct <- grepl("\u2020", ele.prefixes)
          #"-" prefix will be ignored and function will return FASLE if any of 'd', '?', '†' is not present.
          return(any((inc.doubtfully.native & is.doubtfully.native) | (inc.doubtfully.present & is.doubtfully.present) | (inc.extinct & is.extinct)))
        }
      } else { #a particular subregion is given
        find.subregion <- ele.subregions %in% sub.region
        if (any(find.subregion)) {
          if (sum(find.subregion) > 1) {
            stop("CHECK is_native CODE 'case 1' AND RECODE.")
          }
          element <- element[which(find.subregion)] #prefixes will be evaluated after
        } else { #given subregion is not among the ones present in the occurrence string, thus taxa is absent from there
          return(FALSE)
        }
      }
    }
    ele.prefixes <- get_prefixes(element)
    ele.region <- clean_prefixes(get_region(element))
    ele.subregions <- get_subregions(element)
    if (region != ele.region) {
      stop("Unexpected result in 'is_native' function (2).")
      }
    #without subregion
    if (is.null(sub.region)) {
      if (region == element) { #if so, no subregions and no prefixes in occurrence element
        return(TRUE)
      }
      if (ele.prefixes == "") { #no prefixes in occurrence element (even if it has subregions)
        return(TRUE)
      } else { #occurrence element has prefixes (even if it has subregions)
        is.doubtfully.native <- grepl("d", ele.prefixes)
        is.doubtfully.present <- grepl("[?]", ele.prefixes)
        is.extinct <- grepl("\u2020", ele.prefixes)
        #"-" prefix will be ignored and function will return FASLE if any of 'd', '?', '†' is not present.
        return((inc.doubtfully.native & is.doubtfully.native) | (inc.doubtfully.present & is.doubtfully.present) | (inc.extinct & is.extinct))
      }
    } else { #a particular subregion is given
      if (region == clean_prefixes(element) || ele.subregions == "") { #Much probably the two parts of the OR are redundant #Check is this verification is even needed (independent of the possible redundancy)
        return(stop("Unexpected result in 'is_native' function: given region should have subregions in the occurrence string!"))
      }
      if (sub.region %in% ele.subregions) {
        if (ele.prefixes == "") { #no prefixes in occurrence element
          return(TRUE)
        } else { #occurrence element has prefixes
          is.doubtfully.native <- grepl("d", ele.prefixes)
          is.doubtfully.present <- grepl("[?]", ele.prefixes)
          is.extinct <- grepl("\u2020", ele.prefixes)
          #"-" prefix will be ignored and function will return FASLE if any of 'd', '?', '†' is not present.
          return((inc.doubtfully.native & is.doubtfully.native) | (inc.doubtfully.present & is.doubtfully.present) | (inc.extinct & is.extinct))
        }
      }
      return(FALSE)
    }
  } else {return(FALSE)}
}

# given an occurrence string, checks introduction (casual or naturalized) to a region or a region+subregion (with options)
# if present, casual alien ('a') or naturalized ('n') prefixes always return TRUE (as this can not be distinguished from the common case, without prefixes)
is_introduced <- function(occurrence, region, sub.region=NULL, inc.cultivated=FALSE) {
  if (length(occurrence)==0) {return(NULL)}
  if (is.na(occurrence)) {return(NA)}
  if (!region %in% REGIONS) {stop("Given region was not recognized as an EURO+MED region.")}
  if (!is.null(sub.region)) {
    validate_occurrence(paste0(region, "(", sub.region, ")"))
  }
  parts <- validate_occurrence(occurrence)
  if (any(grepl(region, parts$non.native.elements))) {
    element <- grep(region, parts$non.native.elements, value=TRUE)
    if (length(element)>1) {
      #Automatic merging of these cases (migth promote errors!).
      prefixes <- paste0(sapply(element, get_prefixes), collapse="")
      prefixes <- paste0(unique(strsplit(prefixes, split=NULL)[[1]]), collapse="")
      subregions <- sapply(element, get_subregions)
      subregions <- subregions[!subregions==""]
      if (length(subregions)==0) {
        element <- paste0(prefixes, region)
      } else {
        element <- paste0(prefixes, region, "(", paste0(unlist(subregions), collapse=" "), ")")
      }
    }
    ele.prefixes <- get_prefixes(element)
    ele.region <- clean_prefixes(get_region(element))
    ele.subregions <- get_subregions(element)
    if (region != ele.region) {stop("Unexpected result in 'is_introduced' function.")}
    #without subregion
    if (is.null(sub.region)) {
      if (region == element) { #if so, no subregions and no prefixes in occurrence element
        return(TRUE)
      }
      if (ele.prefixes == 	"") { #no prefixes in occurrence element (even if it has subregions)
        return(TRUE)
      } else { #occurrence element has prefixes (even if it has subregions)
        if (grepl("a|n", ele.prefixes)) { #"a" and "n" prefix will return always TRUE
          return(TRUE)
        }
        is.cultivated <- grepl("c", ele.prefixes)
        #other prefixes (not sure if used) will be ignored and function will return FASLE if 'c' is not present.
        return(inc.cultivated & is.cultivated)
      }
    } else { #a particular subregion is given
      if (region == clean_prefixes(element) || ele.subregions == "") { #Much probably the two parts of the OR are redundant #CONFIRMAR SE FAZ SENTIDO ESTA VERIFICAÇÃO (independentemente de ser redundante)!
        return(stop("Unexpected result in 'is_native' function: given region should have subregions in the occurrence string!"))
      }
      if (sub.region %in% ele.subregions) {
        if (ele.prefixes == "") { #no prefixes in occurrence element
          return(TRUE)
        } else { #occurrence element has prefixes
          if (grepl("a|n", ele.prefixes)) { #"a" and "n" prefix will return always TRUE
            return(TRUE)
          }
          is.cultivated <- grepl("c", ele.prefixes)
          #other prefixes (not sure if used) will be ignored and function will return FASLE if 'c' is not present.
          return(inc.cultivated & is.cultivated)
        }
      }
      return(FALSE)
    }
  } else {return(FALSE)}
}

#given a occurrence string, checks presence to a region (independently of being native or introduced), a region+subregion with options
#inc.doubtfully.native, inc.doubtfully.present, inc.extinct are combined using OR
is_present <- function(occurrence, region, sub.region=NULL, inc.doubtfully.native=TRUE, inc.doubtfully.present=TRUE, inc.extinct=FALSE, inc.cultivated=TRUE) {
  return(is_native(occurrence = occurrence, region = region, sub.region = sub.region, inc.doubtfully.native = inc.doubtfully.native, inc.doubtfully.present = inc.doubtfully.present, inc.extinct = inc.extinct) | is_introduced(occurrence = occurrence, region = region, sub.region = sub.region, inc.cultivated = inc.cultivated))
}

# checks if the endemic symbol is present in the occurrence string
is_endemic_of_occurrence_regions <- function (occurrence) {
  return(grepl("\u25cf", occurrence))
}


################################################
#auxiliary functions to Flora iberica functions#
################################################

#auxiliary function that splits authority from reference
split_auth_ref <- function (string) {
  res <- unlist(lapply(string, function (x) { #splits authority from reference
    if (grepl(", ", x)) {
      spl_ind <- regexpr(", ", x)
      if (spl_ind==-1) {return(c(auth=x, ref=""))}
      return(c(auth=substr(x, 1, spl_ind-1), ref=substr(x, spl_ind+2, nchar(x))))
    }
    if (grepl(" in ", x)) {
      spl_ind <- regexpr(" in ", x)
      if (spl_ind==-1) {return(c(auth=x, ref=""))}
      return(c(auth=substr(x, 1, spl_ind-1), ref=substr(x, spl_ind+1, nchar(x))))
    }
    return(c(auth=x, auth_and_ref=""))
  }))
  return(res)
}

#auxiliary function to the function aux.f.synon
aux_f_synon_clean <- function(x, split.by, expected.status) {
  y1 <- strsplit(x, split.by)
  y2 <- lapply(y1, clean_stuff, stuff = c("<([^<>]*)>", "&nbsp;", "\t"))
  y3 <- lapply(y2, clean_whitespaces)[[1]]
  if (length(y3)==2) {
    z1 <- split_auth_ref(y3[2])
    return(c(name=y3[1], auth=z1[1], reference=z1[2], status=expected.status))
  }
  if (length(y3)==3) {
    z1 <- split_auth_ref(y3[3])
    return(c(name=paste0(y3[1], " ", y3[2]), auth=z1[1], reference=z1[2], status=expected.status))
  }
  if (length(y3)<2 | length(y3)>3) {stop("TEMPORARY ERROR")}
}

#auxiliary function to address synonyms
aux_f_synon <- function (x) { #This function is eliminating (by returning NULL) anything that is not recognized. This might be problematic!
  if (grepl("<b><i>", x)) {return(aux_f_synon_clean(x, split.by="</b></i>", expected.status="Accepted"))} #Accepted
  if (grepl("color=#800000", x)) {return(aux_f_synon_clean(x, split.by="</i>", expected.status="Homotypic_synonym"))} #Homotypic_synonym
  if (grepl("color=#008000", x)) {return(aux_f_synon_clean(x, split.by="</i>", expected.status="Heterotypic_synonym"))} #Heterotypic_synonym
  if (grepl("color=#0000FF", x)) {return(aux_f_synon_clean(x, split.by="</i>", expected.status="Priority_name"))} #Prioritario
  if (grepl("[^</b>]</i>", x)) {return(aux_f_synon_clean(x, split.by="</i>", expected.status="Sensu_auct"))} #Sensu #Must be after selection by text color
  return(NULL)
}
